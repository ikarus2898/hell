﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public GameObject camerasparent;
    public float hRotationSpeed = 100f;
    public float vRotationSpeed = 80f;
    public float maxVerticalAngle;
    public float minVerticalAngle;
    public float smoothTime=0.05f;

    private float vCamRotationAngles;
    private float hPlayerRotation;
    private float currentHorizontalVelocity;
    private float currentVerticalVelocity;
    private float targetCamEulers;
    private Vector3 targetCamRotation;


    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        
    }

    public void handleRotation(float hInput, float vInput)
    {
        //get Rotation
        float targetPlayerRotation = hInput * hRotationSpeed * Time.deltaTime;
        targetCamEulers += vInput * vRotationSpeed * Time.deltaTime;

        //player Rotation
        hPlayerRotation = Mathf.SmoothDamp(hPlayerRotation, targetPlayerRotation, ref currentHorizontalVelocity, smoothTime);
        transform.Rotate(0f, hPlayerRotation, 0f);

        //Cam Rotation
        targetCamEulers = Mathf.Clamp(targetCamEulers, minVerticalAngle, maxVerticalAngle);
        vCamRotationAngles = Mathf.SmoothDamp(vCamRotationAngles, targetCamEulers, ref currentVerticalVelocity, smoothTime);

        targetCamRotation.Set(-vCamRotationAngles, 0f, 0f);
        camerasparent.transform.localEulerAngles = targetCamRotation;

    }
}
